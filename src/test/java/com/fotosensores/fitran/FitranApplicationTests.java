package com.fotosensores.fitran;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fotosensores.fitran.infracaotransito.InfracaoTransito;
import com.fotosensores.fitran.infracaotransito.InfracaoTransitoRepository;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = FitranApplication.class)
@WebAppConfiguration
public class FitranApplicationTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));	
	
	private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    
    private List<InfracaoTransito> infracaoTransitoList;
    
    @Autowired
    private InfracaoTransitoRepository infracaoTransitoRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext). build();

        this.infracaoTransitoRepository.deleteAllInBatch();

        if (infracaoTransitoList == null)
        	infracaoTransitoList = new ArrayList<InfracaoTransito>();
        this.infracaoTransitoList.add(infracaoTransitoRepository.save(new InfracaoTransito("AAA-1111","SJC", new Date(1456600000467L))) );
        this.infracaoTransitoList.add(infracaoTransitoRepository.save(new InfracaoTransito("BBB-2222","SP",  new Date(1456622222467L))) );
        this.infracaoTransitoList.add(infracaoTransitoRepository.save(new InfracaoTransito("CCC-3333","RJ",  new Date(1455500000467L))) );
    }
	
    @Test
    public void findInfracoesTransitoBetweenDates() throws Exception {
        mockMvc.perform(get("/infracao/buscar/20160227/20160229"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[0].codigo", is(this.infracaoTransitoList.get(0).getCodigo().intValue())))
            .andExpect(jsonPath("$[0].placa", is("AAA-1111")))
            .andExpect(jsonPath("$[0].local", is("SJC")))
            .andExpect(jsonPath("$[0].dataHora", is("2016-02-27 04:06:40.BRT")))
            .andExpect(jsonPath("$[1].codigo", is(this.infracaoTransitoList.get(1).getCodigo().intValue())))
            .andExpect(jsonPath("$[1].placa", is("BBB-2222")))
            .andExpect(jsonPath("$[1].local", is("SP")))
            .andExpect(jsonPath("$[1].dataHora", is("2016-02-27 10:17:02.BRT")));
    }
    
    @Test
    public void createValidInfracaoTransito() throws Exception {
        String bookmarkJson = json(new InfracaoTransito("AAA-1111","LOCAL",new Date()) );
        this.mockMvc.perform(post("/infracao")
                .contentType(contentType)
                .content(bookmarkJson))
                .andExpect(status().isCreated());
    }
    
    @Test
    public void createInfracaoTransitoWithInvalidDataHora() throws Exception {
        String bookmarkJson = json(new InfracaoTransito("111-BBBB","SJC", new Date(new Date().getTime() + 100000)) );
        this.mockMvc.perform(post("/infracao")
                .contentType(contentType)
                .content(bookmarkJson))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void createInfracaoTransitoWithInvalidPlaca() throws Exception {
        String bookmarkJson = json(new InfracaoTransito("","JSC", new Date(1459911111000L)) );
        this.mockMvc.perform(post("/infracao")
                .contentType(contentType)
                .content(bookmarkJson))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void createInfracaoTransitoWithInvalidLocal() throws Exception {
        String bookmarkJson = json(new InfracaoTransito("AAA-1111","", new Date(1459911111000L)) );
        this.mockMvc.perform(post("/infracao")
                .contentType(contentType)
                .content(bookmarkJson))
                .andExpect(status().isBadRequest());
    }    
     
    
    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
