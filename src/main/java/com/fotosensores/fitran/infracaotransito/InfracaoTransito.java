package com.fotosensores.fitran.infracaotransito;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class InfracaoTransito implements Serializable{

	private static final long serialVersionUID = -8031657900685856846L;
	
	public static final String PLACA_REGEXP = "[A-Z]{3}-[0-9]{4}";

	@Id
	@GeneratedValue
	private Long codigo;
	
	@NotNull
	@NotEmpty
	@Pattern(regexp = PLACA_REGEXP)
	@Column(nullable = false, length=8)
	private String placa;
	
	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String local;
	
	@Past
	@NotNull
	@Column
	private Date dataHora;
	
	public String getPlaca() {
		return placa;
	}


	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Long getCodigo() {
		return codigo;
	}


	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}


	public String getLocal() {
		return local;
	}


	public void setLocal(String local) {
		this.local = local;
	}


	public Date getDataHora() {
		return dataHora;
	}


	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	
	public InfracaoTransito(String placa, String local, Date dataHora){
		this.placa = placa;
		this.local = local;
		this.dataHora = dataHora;
	}
	
	public InfracaoTransito(){
		
	}
}
