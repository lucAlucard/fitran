package com.fotosensores.fitran.infracaotransito;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;


public interface InfracaoTransitoRepository extends JpaRepository<InfracaoTransito, Long>{
	
	Collection<InfracaoTransito> findAllByDataHoraBetween(Date startTime, Date endTime);
	
}
