package com.fotosensores.fitran.infracaotransito;

import java.util.Collection;
import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RequestMapping("/infracao")
@RestController
public class InfracaoTransitoRestController {

	@Autowired
	private InfracaoTransitoRepository infracaoTransitoRepository;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder){
		binder.setValidator(new InfracaoTransitoValidator());
	}
	
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> add(@Valid @RequestBody InfracaoTransito it) {
				InfracaoTransito result = null;
				result = infracaoTransitoRepository.save(new InfracaoTransito(it.getPlaca(),
					it.getLocal(), it.getDataHora()));
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setLocation(ServletUriComponentsBuilder
						.fromCurrentRequest().path("/{id}")
						.buildAndExpand(result.getCodigo()).toUri());
				return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
	}
	
	
	

	@RequestMapping(value = "/buscar/{d1}/{d2}", method = RequestMethod.GET)
	Collection<InfracaoTransito> readInfracaoTransito( @PathVariable("d1") @DateTimeFormat(pattern="yyyyMMdd") Date d1, 
			@PathVariable("d2") @DateTimeFormat(pattern="yyyyMMdd")Date d2) {
		return this.infracaoTransitoRepository.findAllByDataHoraBetween(d1, d2); 
	}

	@RequestMapping(method = RequestMethod.GET)
	Collection<InfracaoTransito> readInfracaoTransito() {
		return this.infracaoTransitoRepository.findAll();
	}

}