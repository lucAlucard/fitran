package com.fotosensores.fitran.infracaotransito;

import java.util.Date;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class InfracaoTransitoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return InfracaoTransito.class.equals(clazz);
	}
	
	@Override
	public void validate(Object target, Errors err) {
		ValidationUtils.rejectIfEmptyOrWhitespace(err, "placa", "placa.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(err, "local", "local.empty");
		InfracaoTransito it = (InfracaoTransito) target;
		
		if (it != null && !it.getPlaca().matches(InfracaoTransito.PLACA_REGEXP)){
			err.rejectValue("placa", "placa.invalid", new Object[] {it.getPlaca()}, "Placa inválida.");
		}
		if (it != null && it.getDataHora().after(new Date())){
			err.rejectValue("dataHora", "dataHora.invalid", new Object[] {it.getDataHora()}, "A data/hora deve ser menor ou igual a data atual.");
		}		
		
	}

}
