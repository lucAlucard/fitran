package com.fotosensores.fitran;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fotosensores.fitran.infracaotransito.InfracaoTransito;
import com.fotosensores.fitran.infracaotransito.InfracaoTransitoRepository;

@SpringBootApplication
public class FitranApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitranApplication.class, args);
	}
	
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
		b.indentOutput(true).dateFormat(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.zzz"));
		return b;
	}	
	
	@Bean
	CommandLineRunner init(InfracaoTransitoRepository infracaoTransitoRepository) {
		List<InfracaoTransito> infracoes = new ArrayList<InfracaoTransito>();
		infracoes.add(new InfracaoTransito("AAA-1111","SJC", new Date(1456600000467L)) );
		infracoes.add(new InfracaoTransito("BBB-2222","SP",  new Date(1456632222467L)) );
		infracoes.add(new InfracaoTransito("CCC-3333","RJ",  new Date(1455500000467L)) );
		return (evt) -> infracoes.forEach(
						a -> {
							System.out.println(a.getDataHora().toString());
							infracaoTransitoRepository.save(a);
						});
	}
	 
}